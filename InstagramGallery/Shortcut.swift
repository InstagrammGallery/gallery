//
//  Shortcut.swift
//  PhotoGallery
//
//  Created by luca bonadonna on 29/08/2017.
//  Copyright © 2017 luca bonadonna. All rights reserved.
//




import UIKit

typealias AnimationParameter = (duration: Double, delay: Double, damping: CGFloat, initialVelocity: CGFloat, options: UIViewAnimationOptions)

/**Returns the device screen size.*/
let screen = UIScreen.main.bounds
/**Returns the AppDelegate.*/
let appDelegate = UIApplication.shared.delegate
/**Returns the NSUserDefaults.*/
let userDefaults = UserDefaults.standard
/**Returns the app's main window.*/
let keyWindow: UIWindow! = UIApplication.shared.keyWindow
/**Returns the local comma character based on the device locale.*/
let localComma: String = String(describing: (Locale.current as NSLocale).object(forKey: NSLocale.Key.decimalSeparator))

/**Cap a value below the specified limit.*/
func cap<T: SignedNumber>(_ n: inout T, below limit: T) {
    if n > limit {
        n = limit
    }
}

/**Cap a value above the specified limit.*/
func cap<T: SignedNumber>(_ n: inout T, above limit: T) {
    if n < limit {
        n = limit
    }
}

/**Run the code inside the block after the specified delay.*/
func delay(_ delay: Double, action: @escaping () -> Void) {
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: action)
}

/**Run the code inside the block in a background thread and calls the completion block in the main thread for UI updates.*/
func performInBackground(_ action: @escaping () -> Void, completion: (() -> Void)? = nil) {
    DispatchQueue.global(qos: .default).async {
        action()
        
        DispatchQueue.main.async(execute: {
            completion?()
        })
    }
}
