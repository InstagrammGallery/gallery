//
//  String.swift
//  PhotoGallery
//
//  Created by luca bonadonna on 29/08/2017.
//  Copyright © 2017 luca bonadonna. All rights reserved.
//


import UIKit

extension String {
    
    /**Returns the string length (does not support Emoji characters, use characters.count instead).*/
    var length: Int {
        return self.characters.count
    }
    
    /**Returns the localized string provided in the Localizable file.*/
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    /**Returns the original string as an Int.*/
    var toInt: Int {
        return Int(self) ?? 0
    }
    
    /**Returns the original string as a Float.*/
    var toFloat: Float {
        return Float(self) ?? 0.0
    }
    
    /**Returns the original string as a Double.*/
    var toDouble: Double {
        return Double(self) ?? 0.0
    }
    
    /**Returns the original string as an UIColor. The original string must be a color representation in hexadecimal format.*/
    var toColor: UIColor {
        return UIColor(self)
    }
    
    /**Change the string with the provided variation if the specified parameter is more than 1.*/
    func forValue(_ value: Int, _ suffix: String) -> String {
        return value > 1 ? self + suffix : self
    }
    
    /**Insert a string in the original string at the specified index.*/
    mutating func insert(_ string:String, atIndex index: Int) {
        self = String(characters.prefix(index)) + string + String(characters.suffix(characters.count-index))
    }
    
    /**Returns a substring of the original string from the beginning of the original string to the specified character index.*/
    func substringToIndex(_ index: Int) -> String {
        return self.substringWithRange(0, index)
    }
    
    /**Returns a substring of the original string from the specified character index to the end of the original string.*/
    func substringFromIndex(_ index: Int) -> String {
        return self.substringWithRange(index, self.length)
    }
    
    /**Returns a substring of the original string with the specified length from the specified initial character index.*/
    func substringWithRange(_ start: Int, _ length: Int) -> String {
        return self.substringWithRange(start: start, end: start + length)
    }
    
    /**Returns the substring of the original string from the specified range.*/
    func substringWithRange(start: Int, end: Int) -> String {
        if start > self.length { return "" }
        else if end < 0 { return "" }
        
        var start = start
        var end = end
        if start < 0 { start = 0 }
        cap(&end, below: self.length)
        let range = self.characters.index(self.startIndex, offsetBy: start) ..< self.characters.index(self.startIndex, offsetBy: end)
        return self.substring(with: range)
    }
    
}
