//
//  Global.swift
//  PhotoGallery
//
//  Created by luca bonadonna on 29/08/2017.
//  Copyright © 2017 luca bonadonna. All rights reserved.
//
import UIKit

struct Global {
    
    struct Size {// size of single screen
        static let width = UIScreen.main.bounds.width/100
        static let height = UIScreen.main.bounds.height/100
    }
    
    struct Color {// color of the application
        static let transparent = UIColor.clear
        static let background = UIColor("#00A663")// ho esteso la classe uicolor apposta per poterlo scrivere cosi
        static let backgroundIntro = UIColor("#4AF765")
        static let btn = UIColor(red: 2/255, green: 2/255, blue: 180/255, alpha: 1)
    }
    
    struct rect{
        static let normalImg = CGRect(x: 0, y: g.Size.height*7, width: g.Size.width*100, height: g.Size.width*100)
        static let largeImg = CGRect(x: 0, y: g.Size.height*7, width: g.Size.width*100, height: g.Size.height*80)
        
    }
}



typealias g = Global
