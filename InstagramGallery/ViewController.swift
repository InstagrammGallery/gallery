//
//  ViewController.swift
//  PhotoGallery
//
//  Created by luca bonadonna on 29/08/2017.
//  Copyright � 2017 luca bonadonna. All rights reserved.
//

import UIKit
import SCLAlertView

var img :[Media] = []
var token = ""
class ViewController: UIViewController, UIWebViewDelegate {
    
    var login : UIWebView!
    let splash = UIView(frame: CGRect(x: 0, y: 0, width: g.Size.width*100, height: g.Size.height*100))
    let nextView = GalleryViewController()
    let appearance = SCLAlertView.SCLAppearance(
        showCloseButton: false
    )
    
    override func viewDidLoad() {
        
        self.splash.backgroundColor = g.Color.background
        self.view.backgroundColor = g.Color.background
        self.loginFunc()
    }
    
    func loginFunc() {
        // effetuo il login tramite una webview precedentemnete istanziata
        let client_Id = "e1434ca35c204c04a9c2c8d3f4200b65"
        let redirect_uri =  "http://www.google.it"
        let site = "https://api.instagram.com/oauth/authorize/?client_id=\(client_Id)&redirect_uri=\(redirect_uri)&response_type=token"
        
        self.login = UIWebView(frame: CGRect(x: 0, y: 0, width: g.Size.width*100, height: g.Size.height*100))
        self.login.backgroundColor = g.Color.backgroundIntro
        self.login.delegate = self
        let url = URL(string: site )
        let requestObj = URLRequest(url: url!);
        self.login.loadRequest(requestObj)
        self.view.addSubview(self.login)
        self.view.addSubview(self.splash)
        let bgImag = UIImageView(image: #imageLiteral(resourceName: "start"))
        self.splash.addSubview(bgImag)
    }
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.mainDocumentURL!.absoluteString
        if url.contains("http://www.google.it/") {// controllo con il mio link di reindizionamento
            
            // controlli per ottenere l'acces token spezzetanto l'utl di base
            let x = url.components(separatedBy: "#")
            let x2 = x[1].components(separatedBy: "=")
            token = x2[1]// salvo il token e passo alla view successiva quella della gallery
            //just a simple allert to advise that you are login; of course it isn't necessary but it is just to advice the user. 
            let allertDb = SCLAlertView(appearance: self.appearance)// pods of new allertView
            allertDb.showSuccess(NSLocalizedString("successAllert", tableName: "Localizable", bundle: Bundle.main, value: "", comment: ""), subTitle: NSLocalizedString("successSubAllert", tableName: "Localizable", bundle: Bundle.main, value: "", comment: ""), duration: 1.3, colorStyle: 0xdddd00,  animationStyle: SCLAnimationStyle.leftToRight)
            
            self.present(self.nextView, animated: true, completion: nil)
            return false
        }
        else{
            return true
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        print(webView.request?.mainDocumentURL?.absoluteString ?? "nada url")
        self.splash.removeFromSuperview()
    }
    
}
