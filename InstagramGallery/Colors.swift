//
//  Colors.swift
//  PhotoGallery
//
//  Created by luca bonadonna on 29/08/2017.
//  Copyright © 2017 luca bonadonna. All rights reserved.
//


import UIKit

extension UIColor {
    
    /**Initialize an UIColor object from the specified hex string.*/
    convenience init(_ hexString: String) {
        self.init(hexString: hexString, alpha: 1.0)
    }
    
    /**Initialize an UIColor object from the specified hex string with the specified alpha value.*/
    convenience init(hexString: String, alpha: Double) {
        var hex = hexString
        if hex.hasPrefix("#") {
            hex = hex.substringFromIndex(1)
        }
        if hex.length == 3 {
            let r = hex.substringToIndex(1)
            let g = hex.substringWithRange(start: 1, end: 2)
            let b = hex.substringFromIndex(2)
            
            hex = r + r + g + g + b + b
        }
        
        let r = hex.substringToIndex(2)
        let g = hex.substringWithRange(start: 2, end: 4)
        let b = hex.substringWithRange(start: 4, end: 6)
        var red: CUnsignedInt = 0
        var green: CUnsignedInt = 0
        var blue: CUnsignedInt = 0
        Scanner(string: r).scanHexInt32(&red)
        Scanner(string: g).scanHexInt32(&green)
        Scanner(string: b).scanHexInt32(&blue)
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: CGFloat(alpha))
    }
    
    /**Returns the original UIColor with the specified alpha value.*/
    func alpha(_ alpha: CGFloat) -> UIColor {
        return withAlphaComponent(alpha)
    }
    
    /**Returns a random UIColor.*/
    static func random() -> UIColor {
        let colors = [UIColor.red, UIColor.green, UIColor.blue, UIColor.yellow, UIColor.cyan, UIColor.magenta]
        return colors[Int(arc4random_uniform(6) + 1)]
    }
    
    /**Returns a lighter variation of the original UIColor.*/
    func lighter() -> UIColor {
        var h:CGFloat = 0.0, s:CGFloat = 0.0, b:CGFloat = 0.0, a:CGFloat = 0.0
        guard getHue(&h, saturation: &s, brightness: &b, alpha: &a) else { return self }
        return UIColor(hue: h, saturation: s, brightness: min(b * 1.3, 1.0), alpha: a)
    }
    
    /**Returns a darker variation of the original UIColor.*/
    func darker() -> UIColor {
        var h:CGFloat = 0.0, s:CGFloat = 0.0, b:CGFloat = 0.0, a:CGFloat = 0.0
        guard getHue(&h, saturation: &s, brightness: &b, alpha: &a) else { return self }
        return UIColor(hue: h, saturation: s, brightness: b * 0.75, alpha: a)
    }
    
}
