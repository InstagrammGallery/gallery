//
//  Media.swift
//  PhotoGallery
//
//  Created by luca bonadonna on 01/09/2017.
//  Copyright © 2017 luca bonadonna. All rights reserved.
//

import Foundation

class Media {// the media that i'm going to work with
    
    var urlImg : String!
    var urlVideo : String!
    var type : String!
    var height : Int!
    var width : Int!
    
    func mediaImg(urlImg:String, type:String, height:Int, width:Int ) {
        self.urlImg = urlImg
        self.type = type
        self.height = height
        self.width = width
    }
    func mediaVideo(urlImg:String,urlVideo:String, type:String, height:Int, width:Int ) {
        self.urlImg = urlImg
        self.urlVideo = urlVideo
        self.type = type
        self.height = height
        self.width = width
    }


}
