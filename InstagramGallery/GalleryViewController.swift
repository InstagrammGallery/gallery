//
//  autentication.swift
//  PhotoGallery
//
//  Created by luca bonadonna on 29/08/2017.
//  Copyright © 2017 luca bonadonna. All rights reserved.
//

import UIKit



import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import AlamofireImage
import AVKit
import AVFoundation
import MediaPlayer


class GalleryViewController: UIViewController, UIScrollViewDelegate {
    
    // variabili di classe
    var imageView : [UIButton] = []
    var ScrollPhoto : UIScrollView!
    var noPhotoView : UIView!
    // solo all onclick
    var bigImmageView : UIView = UIView (frame: CGRect(x: 0, y: 0, width: g.Size.width*100, height: g.Size.height*100))
    var imgb :UIImageView!
    let iconBack = UIImageView (image: #imageLiteral(resourceName: "TouchB"))
    let back = UILabel (frame: CGRect(x: g.Size.width*0, y: g.Size.height*65, width: g.Size.width*100, height: g.Size.height*20))
    
    
    override func viewDidLoad() {
        self.view.backgroundColor = g.Color.background
        // settaggio del header
        let title = UIImageView (frame: CGRect(x: 0, y: 0, width: g.Size.width*100, height: g.Size.height*13.7))
        title.image = #imageLiteral(resourceName: "NameD")
        
        title.frame = CGRect(x: 0, y: 0, width: g.Size.width*100, height: title.frame.height)
        self.view.addSubview(title)
        // settaggio delle scrollview / Body
        self.ScrollPhoto = UIScrollView(frame: CGRect(x: 0, y: g.Size.height*13.7, width: g.Size.width*100, height: g.Size.height*100))
        self.ScrollPhoto.delegate = self
        self.ScrollPhoto.isScrollEnabled = true
        self.ScrollPhoto.showsVerticalScrollIndicator = true
        self.ScrollPhoto.alwaysBounceVertical = false
        self.ScrollPhoto.backgroundColor = g.Color.background
        self.loadImg()
    }

    
    // after login
    func loadImg() {
        let site = "https://api.instagram.com/v1/users/self/media/recent/?access_token=\(token)"//site for catch the photo
        let queue = DispatchQueue(label: "com.cnoon.response-queue", qos: .utility, attributes: [.concurrent])
        // presa delle immagini del singolo utente
        Alamofire.request(site, method: .get).responseSwiftyJSON(queue: queue, options:  .allowFragments, completionHandler: {  response in
            if (response.response?.statusCode == 200) {
                if response.value?["status"].stringValue == "REQUEST_DENIED"{
                    DispatchQueue.main.async {}
                }
                else{
                    let dati = response.value?.dictionaryObject
                    let media = dati?["data"] as! [[String:Any]]
                    for x in media{// look every media
                        let stepMedia = Media()
                        let type = x["type"] as! String
                        if type == "video"{
                            let x1 = x["images"] as![String:[String:Any]]
                            let x2 = x["videos"] as![String:[String:Any]]
                            let h = x1["standard_resolution"]?["height"] as! Int
                            let w = x1["standard_resolution"]?["width"] as! Int
                            let video = x2["standard_resolution"]?["url"] as! String
                            stepMedia.mediaVideo(urlImg: x1["standard_resolution"]?["url"] as! String, urlVideo: video,type: type, height: h, width: w)// remembere video details
                        }
                        else{
                            let x1 = x["images"] as![String:[String:Any]]
                            let h = x1["standard_resolution"]?["height"] as! Int
                            let w = x1["standard_resolution"]?["width"] as! Int
                            stepMedia.mediaImg(urlImg: x1["standard_resolution"]?["url"] as! String, type: type, height: h, width: w)// remembere photo details
                        }
                        img.append(stepMedia)// save media
                    }
                    DispatchQueue.main.async {// operazioni con alamofire ed api concluse
                        if img.count != 0 {
                            // se l'utente ha effetuato alcuna foto o video su instagram
                            var count = 0 // contatore per riga
                            var countB = CGFloat(0)// contatore per altezza
                            let ymin = CGFloat(10)// altezza minima
                            for i in img{
                                let urlImage = URL(string: i.urlImg)
                                let thisa = UIButton(frame: CGRect(x: (g.Size.width*CGFloat(count*33))+(g.Size.width*3), y: g.Size.height*CGFloat(countB*ymin), width: g.Size.height*15, height: g.Size.height*15))
                                thisa.af_setBackgroundImage(for: UIControlState.normal, url: urlImage!)
                                
                                self.imageView.append(thisa)
                                count = count+1
                                if count == 3 {// 3 foto per riga
                                    count = 0
                                    countB = countB + 2
                                }
                            }
                            var n = 0
                            // add views to scrollview
                            for i in self.imageView{
                                i.tag = n
                                i.addTarget(self, action: #selector(self.bigImagefunc(_:)), for: .touchUpInside)
                                self.ScrollPhoto.addSubview(i)
                                n = n+1
                            }
                            self.view.addSubview(self.ScrollPhoto)
                            let hcell = CGFloat((Double(self.imageView.count/15)+0.5)*100)
                            self.ScrollPhoto.contentSize = CGSize(width: g.Size.width*100, height: g.Size.height*hcell)// set size of scrollview's contenent
                        }
                        else{
                            // se l'utente non ha effetuato alcuna foto o video su instagram
                            self.noPhotoView = UIView(frame: CGRect(x: 0, y: g.Size.height*13.7, width: g.Size.width*100, height: g.Size.height*100))
                            self.noPhotoView.backgroundColor = g.Color.background
                            let label = UILabel(frame: CGRect(x: 0, y: g.Size.height*40, width: g.Size.width*100, height: g.Size.height*40))
                            label.numberOfLines = 0
                            label.textAlignment = .center
                            label.text = NSLocalizedString("noPhotoText", tableName: "Localizable", bundle: Bundle.main, value: "", comment: "")
                            self.noPhotoView.addSubview(label)
                            self.view.addSubview(self.noPhotoView)
                        }
                        
                        
                    }
                }
            }
        })
    }
    
    
    func bigImagefunc(_ sender: UIButton) {
        print(sender.tag)
        let choose = img[sender.tag]
        if choose.type == "image" {
            // choose the photo and show it big
            iconBack.frame = CGRect(x: g.Size.width*10, y: g.Size.height*70, width: g.Size.height*10, height: g.Size.height*10)
            iconBack.alpha = 0.7
            self.bigImmageView.addSubview(iconBack)
            self.imgb = UIImageView (frame: g.rect.normalImg)
            self.imgb.sizeThatFits(CGSize(width: choose.width, height: choose.height))
            let urlImage = URL(string: choose.urlImg)
            imgb.af_setImage(withURL: urlImage!)
            self.bigImmageView.addSubview(imgb)
            self.bigImmageView.backgroundColor = g.Color.backgroundIntro
            self.view.addSubview(self.bigImmageView)
            
            let x = NSLocalizedString("back", tableName: "Localizable", bundle: Bundle.main, value: "", comment: "")
            back.text = x
            back.textColor = UIColor.black
            back.textAlignment = .center
            back.numberOfLines = 0
            self.bigImmageView.addSubview(back)
            self.bigImmageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.callBack)))
            
        }
        else{
            // choose the video and show it big
            let url = URL(string: choose.urlVideo)!
            print(choose.urlVideo)
            
            // begin implementing the avplayer
            let player = AVPlayer(url: url)
            
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                player.isMuted = false
                player.play()
            }


        }
    }
    
    func callBack(){
        // return to gallery
        self.imgb.removeFromSuperview()
        self.iconBack.removeFromSuperview()
        self.back.removeFromSuperview()
        self.bigImmageView.removeFromSuperview()
        self.imageView.removeAll()
        if img.count != 0 {
            img.removeAll()
            self.ScrollPhoto.removeFromSuperview()
        }
        else{
            self.noPhotoView.removeFromSuperview()
        }
        self.loadImg()
    }
    
    
}
